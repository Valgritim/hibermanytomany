package org.eclipse.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Sport {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	private String nom;
	private String type;
	
	@ManyToMany(mappedBy="sports")
	private List<Personne> personnes = new ArrayList<Personne>();
	
	public Sport(String nom, String type) {
		super();
		this.nom = nom;
		this.type = type;
	}	

	public Sport(String nom, String type, List<Personne> personnes) {
		super();
		this.nom = nom;
		this.type = type;
		this.personnes = personnes;
	}

	public Sport() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public List<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(List<Personne> personnes) {
		this.personnes = personnes;
	}
	
	

	public void addPersonne(Personne personne) {
		personnes.add(personne);
		personne.getSports().add(this);
	}

	public void removePersonne(Personne personne) {
		personnes.remove(personne);
		personne.getSports().remove(this);
	}

	@Override
	public String toString() {
		return "Sport [id=" + id + ", nom=" + nom + ", type=" + type + "]";
	}
	
	
}
