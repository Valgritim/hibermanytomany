package org.eclipse.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Personne {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	private String prenom;
	private String nom;
	
	@ManyToMany(cascade= {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<Sport> sports = new ArrayList<Sport>();
	
	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.REMOVE})
	//@JoinColumn(name="rue", referencedColumnName="rue", nullable=false)
	private List<Adresse> adresses = new ArrayList<Adresse>();

	public Personne(String prenom, String nom, List<Sport> sports, List<Adresse> adresses) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.sports = sports;
		this.adresses = adresses;
	}

	public Personne() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public List<Sport> getSports() {
		return sports;
	}

	public void setSport(List<Sport> sports) {
		this.sports = sports;
	}
	
	

	public void addSport(Sport sport) {
		sports.add(sport);
		sport.getPersonnes().add(this);
	}

	public void remove(Sport sport) {
		sports.remove(sport);
		sport.getPersonnes().remove(this);
	}

	public List<Adresse> getAdresses() {
		return adresses;
	}

	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}
	
	

	public boolean addAdresse(Adresse e) {
		return adresses.add(e);
	}

	public Adresse removeAdresse(int index) {
		return adresses.remove(index);
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", sports=" + sports + "]";
	}
	
	
}
