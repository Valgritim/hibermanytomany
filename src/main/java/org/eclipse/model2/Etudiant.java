package org.eclipse.model2;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="ETU")
public class Etudiant extends Person{
	
	private String niveau;


	public Etudiant() {
		super();
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return "Etudiant [niveau=" + niveau + "]";
	}


	
	
	
	
}
