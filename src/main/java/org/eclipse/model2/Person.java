package org.eclipse.model2;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE) //une seule table sera créée avec une colonne type_person qui indiquera PERS ETU OU ENS selon ce qu'on a créé
//si je veux une création pour chaque classe qui érite de person je dois indique strategy=InheritanceType.TABLE_PER_CLASS
@DiscriminatorColumn(name="TYPE_PERSON")
@DiscriminatorValue(value="PERS")
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	private String prenom;
	private String nom;	

	public Person() {
		super();
	}

	public Person(String prenom, String nom) {
		super();
		this.prenom = prenom;
		this.nom = nom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", prenom=" + prenom + ", nom=" + nom + "]";
	}
	
	
	
	
}
