package org.eclipse.model2;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="ENS")
public class Enseignant extends Person{

	private int salaire;

	public Enseignant() {
		super();
	}

	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	@Override
	public String toString() {
		return "Enseignant [salaire=" + salaire + "]";
	}
	
	
}
