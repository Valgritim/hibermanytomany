package org.eclipse.main;



import java.util.ArrayList;
import java.util.List;

import org.eclipse.model.Personne;
import org.eclipse.model.Sport;
import org.eclipse.model2.Enseignant;
import org.eclipse.model2.Etudiant;
import org.eclipse.model2.Person;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        
//        Personne p1 = new Personne();
//        Personne p2 = new Personne();
//        
//        p1.setNom("Voigt");
//        p1.setPrenom("Bill");
//        p2.setNom("Gates");
//        p2.setPrenom("Bill");
//        
//        Sport s1 = new Sport();
//        Sport s2 = new Sport();
//        Sport s3 = new Sport();
//        s1.setNom("Football");
//        s2.setNom("Rugby");
//        s3.setNom("Tennis");
//        s1.setType("collectif");
//        s2.setType("collectif");
//        s3.setType("individuel");
//        
//        p1.addSport(s1);
//        p1.addSport(s3);
//        p2.addSport(s2);
//        p2.addSport(s3);
//        
//        session.persist(p1);
//        session.persist(p2);
//        transaction.commit();
//        session.close();
//        sessionFactory.close();
//        
//        for(Personne personne:s1.getPersonnes())
//        	System.out.println(personne.getNom());
        
//          String hql = "SELECT p FROM Personne p where prenom = :prenom";
//          String string = "Bill";
//          Query query = session.createQuery(hql);
//          query.setParameter("prenom", string);
//          List<Personne> personnes = query.list();
//          for(Personne personne: personnes)
//        	  System.out.println(personne);
//          
//          transaction.commit();
//          session.close();
//          sessionFactory.close();
        
        Person person = new Person();
        person.setNom("Gardiola");
        person.setPrenom("Pep");
        
        Enseignant enseignant = new Enseignant();
        enseignant.setNom("Ferguson");
        enseignant.setPrenom("Siry");
        enseignant.setSalaire(10000);
        
        Etudiant etudiant = new Etudiant();
        etudiant.setNom("Mourinho");
        etudiant.setPrenom("Jose");
        etudiant.setNiveau("Ligue 1");
        
        //Persistance
        session.persist(person);
        session.persist(enseignant);
        session.persist(etudiant);        
        transaction.commit();
        session.close();
        sessionFactory.close();
    }
}
